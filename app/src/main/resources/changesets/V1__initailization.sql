CREATE TABLE items
(
   id          SERIAL            PRIMARY KEY,
   name        TEXT              NOT NULL,
   type        TEXT              NOT NULL,
   value       DOUBLE PRECISION  NOT NULL,
   measurement TEXT              NOT NULL
);

CREATE TABLE users
(
   id                SERIAL   PRIMARY KEY,
   username          TEXT     NOT NULL,
   hashed_password   TEXT     NOT NULL,
   email             TEXT     NOT NULL
);
