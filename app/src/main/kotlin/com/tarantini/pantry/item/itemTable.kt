package com.tarantini.pantry.item

import com.tarantini.pantry.datastore.Table

object ItemTable: Table {
   enum class Columns(val label: String) {
      NAME("name"),
      TYPE("type"),
      VALUE("value"),
      MEASUREMENT("measurement");
   }

   override val tableName: String
      get() = "items"

   override fun getColumns(): List<String> {
      return Columns.values().map { it.label }.toList()
   }
}
