package com.tarantini.pantry.item

import com.tarantini.pantry.domain.CreateItemRequest
import com.tarantini.pantry.endpoints.withPathParam
import com.tarantini.pantry.endpoints.withRequest
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.itemEndpoints(service: ItemService) {
   get("/v1/users/{userId}/item") {
      withPathParam<Long>("userId") { userId ->
         service.findAllByUser(userId).fold(
            { call.respond(HttpStatusCode.OK, it) },
            { call.respond(HttpStatusCode.InternalServerError, it) }
         )
      }
   }

   post("/v1/item") {
      withRequest<CreateItemRequest> {
         service.create(it.name, it.type, it.weight, it.measurement).fold(
            { call.respond(HttpStatusCode.OK, it) },
            { call.respond(HttpStatusCode.InternalServerError) }
         )
      }
   }
}
