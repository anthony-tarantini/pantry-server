package com.tarantini.pantry.user

import com.tarantini.pantry.domain.CreateUserRequest
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.respond
import io.ktor.server.routing.*
import io.ktor.util.pipeline.*

fun Route.userEndpoints(service: UserService) {
   get("/v1/user") {
      service.all().fold(
         { call.respond(HttpStatusCode.OK, it) },
         { call.respond(HttpStatusCode.InternalServerError, it) }
      )
   }

   post("/v1/user") {
      withCreateUserRequest {
         service.create(it.username, it.password, it.email).fold(
            { call.respond(HttpStatusCode.OK, it) },
            { call.respond(HttpStatusCode.InternalServerError) }
         )
      }
   }
}

private suspend fun PipelineContext<Unit, ApplicationCall>.withCreateUserRequest(f: suspend (CreateUserRequest) -> Unit) {
   runCatching { call.receive<CreateUserRequest>() }
      .fold(
         { f(it) },
         { call.respond(HttpStatusCode.BadRequest, it) }
      )
}
