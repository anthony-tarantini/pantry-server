package com.tarantini.pantry.domain

enum class ItemType { Dry, Wet }
data class Weight(val value: Double, val measurement: Measurement)
enum class Measurement {
   GRAM, KILOGRAM,
}
data class Item(
   val name: String,
   val type: ItemType,
   val weight: Weight,
)
data class CreateItemRequest(
   val name: String,
   val type: ItemType,
   val weight: Double,
   val measurement: Measurement
)
